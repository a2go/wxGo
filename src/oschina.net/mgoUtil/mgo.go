package mgoUtil

import (
	"github.com/jinzhu/configor"
	"github.com/labstack/gommon/log"
	"gopkg.in/mgo.v2"
)

//AppConfig 应用配置文件对应的实体
var AppConfig = struct {
	//应用名称
	APPName string `default:"微信应用配置文件"`
	//Mongo Mongodb的配置信息
	MgoDB struct {
		//主机地址
		Host string
		//端口号
		Port string
		//用户名
		User string
		//密码
		Pass string
	}
}{}

//init 主函数包初始化函数
func init() {
	//读取配置信息
	err := configor.Load(&AppConfig, "config.yml")
	if nil != err {
		log.Error(err.Error())
		panic("配置文件出错，请检查后重试!,请情参考:" + err.Error())
	}

}

var MgoSession *mgo.Session

func RegisterMgoDB() (err error) {
	MgoSession, err = mgo.Dial(AppConfig.MgoDB.Host)
	return err
}
