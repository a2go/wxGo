package wechat

import (
	"github.com/labstack/echo"
	"oschina.net/mfile/wechat/hand"
)

// InitWeChatRouter 初始化微信路由
func InitWeChatRouter(echo *echo.Echo) {
	//微信路由分组
	wGroup := echo.Group("wechat")
	//验证微信公众号
	wGroup.GET("/hand", hand.WeChatValid)
	//处理微信请求
	wGroup.POST("/hand", hand.WeChatProcess)
}
