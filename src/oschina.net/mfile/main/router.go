package main

import (
	"github.com/labstack/echo"
	"oschina.net/mfile/wechat"
)

//InitRouter 初始化路由
func InitRouter(e *echo.Echo) {
	//引入微信路由
	wechat.InitWeChatRouter(e)
}
