package main

import (
	"github.com/labstack/echo"
	"oschina.net/mgoUtil"
)

// main 主程序入口
func main() {
	//初始化数据库连接
	err := mgoUtil.RegisterMgoDB()
	if nil != err {
		panic("数据库连接失败，详情参考：" + err.Error())
	}
	//启动Echo监听
	es := echo.New()
	//载入路由
	InitRouter(es)
	es.Start(":1237")
}
