package hand

import (
	"net/http"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
	"oschina.net/mgoUtil"
)

//WeChatValid 验证微信公众号接入
func WeChatValid(c echo.Context) (err error) {

	//i, err := mgoUtil.MgoSession.DB("test").C("people").Count()

	var res interface{}
	err = mgoUtil.MgoSession.DB("test").C("people").FindId(bson.ObjectIdHex("58f9b6a2c928fa51fe824090")).One(&res)
	if nil != err {
		return err
	}
	c.JSON(http.StatusOK, res)
	return err
}

//WeChatProcess 接收微信公众号消息并处理
func WeChatProcess(c echo.Context) (err error) {
	c.JSON(http.StatusOK, "处理微信")
	return err
}
